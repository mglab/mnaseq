#' @name binAvg
#' @title Binned average from bedgraph file
#' 
#' @param file Directory containing bedgraph files
#' @param format Data format 'BigWig' or 'bedGraph'.
#' @param binsize Size of a bin. Default value is 100 bp.
#' @details Calculate binned average from files containing signal coverages, e.g. in bigwig or bedGraph formats.
#' 
#' @return Vector of binned average scores.
#' 
#' @export

binAvg <- function(file = NULL,
                   format = 'BigWig',
                   binsize = 50)
{
    # Import file to GRanges
    signal <- import(file, format=format)

    # Sort GRange
    signal <- sort(GenomeInfoDb::sortSeqlevels(signal))
    
    # Find coverage of GRange
    score <- GenomicRanges::coverage(signal, weight="score")
    
    # Get bins
    bins <- GenomicRanges::tileGenome(seqinfo(BSgenome.Scerevisiae.UCSC.sacCer3),
                                      tilewidth = binsize,
                                      cut.last.tile.in.chrom = TRUE)
    
    # Select bins for seqnames in signal
    #bins <- bins[seqnames(bins) %in% seqlevels(signal)]
    bins <- subsetByOverlaps(bins, signal)
    seqlevels(bins) <- seqlevels(signal)
    
    # Calculate binned average
    binned_data <- GenomicRanges::binnedAverage(bins, score, "average_score")
    
    # Vector of average score
    bin_avg <- mcols(binned_data)$average_score  
    
    return(bin_avg)
}
