#' Generate coverages rda and bw from bam files, then convert coverages to 
#'
#' @param bamdir Input directory containing bam files to process. Required
#' @param rdadir Output directory to save rda files containing coverages.
#' @param type 'SE' for single end reads and 'PE for pair-end reads. Default value is 'PE'. Optional.
#' @param ORFs Tab delimited text file containing genome annotation. Optional.
#' @param cores Number of available for calculation threads. Optional.
#'             Default value is "PE". Optional.
#' @param chromosomes Choose chromosomes if necessary; e.g c('chrI','chrII',
#'                    'chrIII'). Default NULL. Optional.
#' @param average_fraglength Expected average fragment length. Only for SE 
#'                           sequences to guess center position. Default value is 
#'                           150. Optional.
#' @param min_fraglength Minimal fragment length. Default value is 130. Optional
#' @param max_fraglength Maximal fragment length. Default value is 160. Optional.
#' @param subsample_number Number of reads taken for compair samples with each other. 
#'                         Default 6 mlns. Optional.
#' @param subsample_withreplacement Logical if replacement allowed for subsampling. 
#'                                  Default value is TRUE. Optional.
#' @param smooth_width Width of smoothing window. Default value is 50. Optional.
#' @param divide_bytotal. Logical. Default value is TRUE. Optional.
#' @param beforeRef bps before reference to take for shift estimation. Default 
#'                  value is 200. Optional.
#' @param afterRef bps after reference to take for shift estimation. Default value 
#'                 is 800. Optional.
#' @param smoothingWindow Size of the smoothing window. Default value is 75. Optional.
#' @param spacing.low Minimum spacing allowed. Default value is 130. Optional.
#' @param spacing.high Maximum spacing allowed. Default value is 220. Optional.
#' @param shift.low Minimum shift allowed. Default value is -70. Optional.
#' @param shift.high Maximum shift allowed. Default value is 60. Optional.
#' 
#' @return NULL
#' 
#' @seealso \code{\link{bam2bwr}}, \code{\link{tssCentered Density}}, 
#'          \code{\link{nrl}}, \code{\link{bam2dyad}}
#'
#' @export

bam2rda <- function ( ... ) {
    
    # Convert bam files to coverages bigwig and rda files.
    mnaseq::bam2bwr( ... )
    
    # Calculate +1 nucleosomes centered dyad density from rda coverages.
    mnaseq::tssCenteredDensity( ... )

    # Calculate ARS centered dyad density from rda coverages.
    mnaseq::tssCenteredDensity( feature = "ARS", ... )

    # Calculate tss centered dyad density from rda coverages.
    mnaseq::tssCenteredDensity( feature = "IntConv", ... )
    
    # Calculate Ocampo data from rda coverages.
    mnaseq::nrl( ... )
    
}
