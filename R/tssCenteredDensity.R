#' Caclulate nucleosome tss centered densities and save them into rda.
#'
#' @param rdadir Input direcotry containing tss centered rda matrices.
#' @param annotation Annotation Input file with features. If not provided, then 
#'                   inner annotation files will be used. Default value is NULL.
#'                   Four columns are required:
#'                   1. 'Feature_names' (the first column), e.g. gene names.
#'                   2. 'Chr' chromosome.
#'                   3. 'Genomic coordinate' for centers of feature which is specified 
#'                   by 'feature' argument (e.g. ARS or TSS)
#'                   4. 'Strand' for DNA strand: "-1" for inversed features.
#' @param feature Centered feature. Presets:
#'                1. "Plus1"   - for centered at +1 nucleosome, 
#'                2. "TSS"     - for centers at Trascription Start Sites, 
#'                3. "TTS"     - for centers at Transcription Termination Sites, 
#'                4. "ARS"     - for centers at Autonomously Replicating Sequences, 
#'                5. "Minus1"  - for centers at -1 nucleosomes, 
#'                6. "NDR"     - for centers at Nucleosome Depleted Regions.
#'                7. "IntConv" - for centers at Intergenic Convergent Regions.
#'                Custom feature can be used when annotation file is 
#'                specified by 'annotation' argument. In this case 'feature' is 
#'                the column name in 'annotation' file and represent centered 
#'                genomic coordinats. Default value is "Plus1".
#' @param min_gene_length Minimal gene length to select. Applied only for genes 
#'                        when feature: "Plus1", "TSS", "TTS", "Minus1", "NDR" is
#'                        selected. Otherwise this parameter will be ignored. Default
#'                        value is 1500.
#' @param max_gene_length Maximal gene length to select. Applied only for genes 
#'                        when feature: "Plus1", "TSS", "TTS", "Minus1", "NDR" is
#'                        selected. Otherwise this parameter will be ignored. Default
#'                        value is 500.
#' @param cores Number of processor threads using for the analysis. Default value 
#'              is 2.
#' @param window.size The size of the window surrounding the center position. 
#'                    Default value is 2000.
#' 
#' @seealso \code{\link{coverageWindowsCenteredStranded}}
#'
#' @export

tssCenteredDensity <- function(rdadir,
                               annotation = NULL, 
                               feature = "Plus1",
                               min_gene_length = 500,
                               max_gene_length = 1500,
                               cores = 2L,
                               ...) {
    
    #TODO: make separate function for feature selection
    if(is.null(annotation)) {
        if (feature %in% c("Plus1", "TSS", "TTS", "Minus1", "NDR")) {
            annotation <- paste0(find.package('mnaseq'), '/extdata/annotations/', "sacCer3_ORFs.csv")
        } else if (feature == "ARS") {
            annotation <- paste0(find.package('mnaseq'), '/extdata/annotations/', "sacCer3_ARSs_centers.csv")
        } else if (feature == "IntConv") {
            annotation <- paste0(find.package('mnaseq'), '/extdata/annotations/', "sacCer3_intergenic_convergent_centers.csv")
        } else {
            stop("\"feature\" is not detected. Please specify correct \"feature\" or provide corresponding \"annotation\" file.\n")
        }
    } else {
      if (!file.exists(annotation)) {
        stop("Annotation file not found. Please specify \"annotation\" file containing genome annotations.\n") }
    }

    # read annotation
    anno <- read.table(annotation, header = TRUE, sep = "\t", quote = "")
    
    # select genes sizes
    if (feature %in% c("Plus1", "TSS", "TTS", "Minus1", "NDR")) {
      anno$gene_length <- abs(anno$ORF_End - anno$ORF_Start)
      anno <- anno[(anno$gene_length >= min_gene_length) & (anno$gene_length <= max_gene_length),]
    }
    
    # centers frame
    centers <- data.frame(
        chr = anno$Chr,
        center = anno[[feature]],
        strand = ifelse(anno$Strand == "-1", "-", "+")
    )
    # first column is the feature name
    rownames(centers) <- anno[,1]

    # list of rda files containing coverages
    covs <- list.files(rdadir, pattern = "^coverage_.+.rda$", full.names = TRUE)
    
    # load coverages
    for (i in seq_along(covs)) {
        load(covs[i])
    }
    
    # remove extension
    covs <- basename(gsub(".rda", "", covs))
    
    # processing to make smooth matrix
    process_mat <- function(i) {
        name <- paste0("smooth.mat.", feature, ".", gsub("coverage_", "", covs[i]))
        
        mat <- mnaseq::coverageWindowsCenteredStranded(centers = centers,
                                                       coverage = get(covs[i]),
                                                       ...)
        
        assign(name, mat)
        
        save(list = name,
             file = paste0(rdadir, "/", name, ".rda"))
        
        print(paste(name, "created"))
    }
    
    # parallel processing
    # windows does not support mc.cores > 1, use lapply instead of mclapply
    # if( .Platform$OS.type == "windows" ){
    #     lapply(seq_along(covs),
    #            FUN = process_mat
    #     )
    # } else {
    # parallel::mclapply(
    #     seq_along(covs),
    #     mc.cores = cores,
    #     FUN = process_mat
    # ) }

    lapply(seq_along(covs), FUN = process_mat)
}
