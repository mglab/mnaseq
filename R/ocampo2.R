#' Get ocampo parameters per instance (Tamas version using ccv)
#'
#' @param coverage a coverage object (\code{\link[IRanges]{RleList}} as returned by \code{\link[IRanges]{coverage}})
#' @param references reference regions data frame with columns (chr, start, end, strand). rownames will serve as id for output table
#' @param beforeRef bps before reference to take for shift estimation. Default 
#'                  value is 200.
#' @param afterRef bps after reference to take for shift estimation. Default value 
#'                 is 800.
#' @param smoothingWindow Size of the smoothing window. Default value is 75.
#' @param spacing.low Minimum spacing allowed. Default value is 130.
#' @param spacing.high Maximum spacing allowed. Default value is 220.
#' @param shift.low Minimum shift allowed. Default value is -70.
#' @param shift.high Maximum shift allowed. Default value is 60.
#' @param sigma_scaled Scaling sigma for array model (recommended). Default FALSE
#' @param cores Number of processor threads using for the analysis. Default value 
#'              is 2.
#'
#' @return a data frame with columns r (correlation), space, and shift for each reference provided
#'
#' @export

ocampo2 <- function (coverage, 
                     references, 
                     beforeRef = 200, 
                     afterRef = 800, 
                     smoothingWindow = 75, 
                     spacing.low = 130, 
                     spacing.high = 220, 
                     shift.low = -70, 
                     shift.high = 60, 
                     cores = 2L, 
                     sigma_scaled = FALSE,
                     ... ) 
{
  
  gaussmf <- function(x, sigma, mean) {
    height <- 1
    mfVals = (height * exp(-(((x - mean)^2)/(2 * (sigma^2)))))
  }
  
  Pattern <- list()
  
  if(sigma_scaled){
    for (d in spacing.low:spacing.high) {
      Pattern[[d]] <- apply(sapply(0:9, function(x) {
        ### !!! scale sigma !!! ###
        gaussmf(seq(-beforeRef, afterRef), 40*(d/150), x * d)
      }), 1, sum)
    }
  } else {
    for (d in spacing.low:spacing.high) {
      Pattern[[d]] <- apply(sapply(0:9, function(x) {
        ### !!! scale sigma !!! ###
        gaussmf(seq(-beforeRef, afterRef), 40, x * d)
      }), 1, sum)
    }
  }
  
  windows <- data.frame(
      chr = references$chr, 
      start = ifelse(references$strand == "+", 
                     references$start - (beforeRef + (smoothingWindow/2)), 
                     references$end - (afterRef + (smoothingWindow/2))), 
      end = ifelse(references$strand == "+", 
                   references$start + (afterRef + (-1 + smoothingWindow/2)), 
                   references$end + (beforeRef + (-1 + smoothingWindow/2))), 
                   strand = references$strand)
  rownames(windows) <- rownames(references)
  mat <- coverageWindowsStranded(windows, coverage)
  
  # Processing matrix
  proc_mat <- function(ridx) {
      rx <- mat[ridx, ]
      x <- zoo::rollmean(rx, smoothingWindow)
      
      bestR <- 0
      spacingV <- NA
      shiftV <- NA
      
      if (round(sd(x), 3) != 0) {
        for (d in spacing.low:spacing.high) {
        
          #
          # This part is changed by Ashish >>>>
        
          y <- Pattern[[d]]
        
          my_ccf <- ccf(x,y, lag.max = abs(shift.low), plot=FALSE)
        
          r <- max(my_ccf$acf)
          shift <- my_ccf$lag[my_ccf$acf == r]
        
          if (r > bestR) {
              bestR <- r
              shiftV <- shift
              spacingV <- d
          
          }
        
        # This part is changed by Ashish <<<<<
        #
        }
      }
    c(r = round(bestR, 2), space = spacingV, shift = shiftV)
  }
  
  
  
  # windows does not support mc.cores > 1, use lapply instead of mclapply
  if( .Platform$OS.type == "windows" ){
    res <- lapply(1:nrow(mat),
                  FUN = proc_mat
                  )
  } else {
    res <- parallel::mclapply(1:nrow(mat),
                              mc.cores = cores,
                              FUN = proc_mat
                              )
  }
  
  df <- t(as.data.frame(res))
  rownames(df) <- rownames(mat)
  df
}
