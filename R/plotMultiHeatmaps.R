#' @name plotMultiHeatmaps
#' @title Plot many heatmaps densities from input matrix.
#'
#' @param mx Input matrix with data in columns and colnames for labels.
#' @param outdir Output directory for saving plot. Default is NULL.
#' @param format "pdf" to save formatted plot in the pdf format.
#' @param plotname Name of plot to save.
#' @param width Plot width in mm. Default value is 80.
#' @param height Plot height in mm.
#' @param pointsize Font size in pt. Default value is 6 pt.
#' @param xlim Range of centered coodinates: c(min, max). Default value is NULL.
#' @param ylim Min and max values limits: c(min, max). Default value is NULL.
#' @param at Position of x-axis labels. Complete length is from 0 to 1.
#' @param xlabels Labels of x-axis which is linked to number of at.
#' @param mam Margins in mm.
#' @param ... Optional graphical parameters: lwd, tck, mgp, ...
#'
#' @export

plotMultiHeatmaps <- function (mx = NULL,
                               format = NULL,
                               plotname = NULL,
                               outdir = NULL,
                               width = 80,
                               height = NULL,
                               pointsize = 6,
                               xlim = NULL,
                               ylim = NULL,
                               xlabels = NULL,
                               at = NULL,
                               xlab = NULL,
                               mam = c(1,20,0,5),
                               lwd = 0.5,
                               tck = -0.02,
                               mgp = c(1.6,0.5,0),
                               ... )
{
    # set colnames if missing
    if(is.null(colnames(mx))) {
            colnames(mx) <- seq(1, ncol(mx), by=1) 
    }
    
    # xlim
    if(is.null(xlim)) {
        xrange = as.numeric(colnames(mx))
        xlim <- c(min(xrange), max(xrange))
    }

    if(is.null(ylim)) {
        ylim <- c(min(mx, na.rm = TRUE), max(mx, na.rm = TRUE))
    }
    
    # select coordinates by xlim
    mx <- mx[, as.character(xlim[1]:xlim[2]), drop = FALSE]
    
    # coordinate markers
    if (is.null(at)) {
        atx = seq(xlim[1], xlim[2], by=250)
        at = ( atx - xlim[1]) / ( xlim[2] - xlim[1] ) }
    if (is.null(xlabels)) xlabels = as.character(atx)

    # labels
    labels = rownames(mx)

    # how many plots
    pl = nrow(mx)
       
    if (!is.null(format) && format == 'pdf') {
        
        if(is.null(height)) height = 10*pl
        
        width.in <- mm2in(width)
        height.in <- mm2in(height)
    
        dir.create(outdir, showWarnings = FALSE)
    
        pdf(paste0(outdir, "/", plotname, ".pdf"),
        width = width.in,
        height = height.in,
        pointsize = pointsize)
    }
    
    # layout
    lmx <- matrix(0,
                  ncol=2,
                  nrow=pl)
    
    lmx[,1] <- seq(1,pl)
    lmx[1:ceiling(pl/2),2] <- pl+1
    
    layout(lmx,
           width = c(9/10, 1/10)
    )
    
    # heatmaps color scheme
    col = colorRamps::matlab.like(100)
    
    # graphical parameters
    mai = mm2in(mam)
    par(mai = mai, oma=c(5,1,1,1), lwd = lwd)
    
    for(i in 1:nrow(mx)){
        image(as.matrix(mx[i,]),
            col = col,
            breaks =  seq(ylim[1], ylim[2], length.out = 101),
            axes = FALSE,
            useRaster = TRUE
           )
        text(
            x = -0.01,
            y = 0.5,
            labels = labels[i],
            adj = c(1,0.5),
            xpd = NA
        )
    }

    # Axes
    axis(
        side = 1, 
        lwd = lwd, 
        las = 1, 
        tck = tck, 
        mgp = mgp,
        at = at,
        labels = xlabels
    )
    
    mtext(side=1, text = xlab, line=2)
    
    # Scale
    mam = c(1,0,5,10)
    mai = mm2in(mam)
    par(mai = mai, lwd = lwd)

    image((matrix(seq(ylim[1], ylim[2], length.out = 101), 1)),
          col = col,
          breaks = seq(ylim[1], ylim[2], length.out = 101),
          yaxt="n",
          xaxt="n"
    )
    
    axis(side = 4,
         las = 1,
         at = seq(0, 1, length.out = 3),
         labels = round(seq(ylim[1], ylim[2], length.out = 3), 1),
         lwd = 0.5,
         )
    
    box(lwd = lwd)
    
    if (!is.null(format) && format == 'pdf') dev.off()
}
