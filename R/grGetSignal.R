#' Get signal
#'
#' @description Get signal from metacolumn of genomic ranges based on given chromosome, start and end genomic coordinates.
#' Return vector containing signal per nucleotide within a certain region.
#'
#' @param gr Genomic Range containing metacolumn.
#' @param chr Chromosome (e.g. "chrI").
#' @param start Genomic coordinate of the start position.
#' @param end Genomic coordinate of the end position.
#'
#' @seealso \code{\link{covPlot}}
#' @return Vector of values.
#' @export

grGetSignal <- function(gr, chr, start, end) {
    if (!is.character(chr))
        stop("\"chr\" must be a character.")
    if (!is.numeric(start) |
        !is.numeric(end))
        stop("\"start\" and \"end\" genomic coordinates must be numeric.")
    if (end < start)
        stop("\"end\" must be greater than \"start\" coordinate.")
    
    region <- GRanges(chr, IRanges(start, end))
    ovlp <- findOverlaps(region, gr)
    gvalue <- gr[subjectHits(ovlp)]
    border <- region[queryHits(ovlp)]
    start(gvalue[1]) = start(border[1])
    end(gvalue[length(gvalue)]) = end(border[length(gvalue)])
    vec <- rep(gvalue$score, width(gvalue))
    return(vec)
}

