#' @name plotFragLength
#' @title Plot fragment length destibution
#'
#' @param bamdir Input director-y/-ies containing meandat files (xxx.mean.dat.gz). Files within one directory may be split by groups/quantiles etc.
#' @param outdir Output directory for saving plot.
#' @param format "pdf" to save formatted plot in the pdf format.
#' @param sample_sheet Sample sheet - tab delimited txt file: <File_names><Lable_names><Colors>.
#' @param subset Vector of names for subsetting by either File_names, Lable_names or as a numeric vector.
#' @param chromosomes Choose chromosomes to calculate coverage. NULL by default.
#' @param min_fraglength Minimal fragment length. 50 by default.
#' @param max_fraglength Maximal fragment length. 350 by default.
#' @param ylim Limit of y-axis. NULL by default, calculated from data.
#' @param width Plot width in mm.
#' @param height Plot height in mm.
#' @param pointsize Font size in pt.
#' @param legend_pos Preset positions of legend 'topleft', 'topright' etc.
#' @param box Logical value if box around plot should be drawn.
#'
#' @export

plotFragLength <- function(bamdir = NULL,
                           outdir = NULL,
                           format = NULL,
                           plotname = "fragments_length_density",
                           sample_sheet = NULL,
                           subset = NULL,
                           chromosomes = NULL,
                           min_fraglength = 50,
                           max_fraglength = 350,
                           width = 50,
                           height = 40,
                           pointsize = 6,
                           ylim = NULL,
                           legend_pos = 'topright',
                           box = TRUE ) {
    # get files
    samples <- mnaseq::getFiles(indir = bamdir,
                                sample_sheet = sample_sheet,
                                subset = subset,
                                prefix = "",
                                extension = "bam"
                               )
    bams <- samples$files
    lables <- samples$lables
    colors <- samples$colors

    frag_lengths <- lapply(bams, function(x) { mnaseq::fragLength(x) })

    # xlim
    xlim = c(min_fraglength, max_fraglength)

    # ylim
    if (is.null(ylim)) {
      ylim <- c(0, 1.05*max(sapply(frag_lengths, function(x) {density(x)$y})))
    }

    if (!is.null(format) && format == 'pdf') {
        mm2in <- function(x) {x*0.03937007874015748}
        width.in <- mm2in(width)
        height.in <- mm2in(height)

        dir.create(outdir, showWarnings = FALSE)

        pdf(paste0(outdir, "/", plotname, ".pdf"),
        width = width.in,
        height = height.in,
        pointsize = pointsize)

        mai = c(8,10,2,5)
        mai.i = mm2in(mai)
        par(mai = mai.i, lwd = 0.5)
    }

    if (!(is.null(sample_sheet))) {
        ## Plot all samples in one plot
        plot.new()
        plot.window(
            xlim = xlim,
            ylim = ylim,
            yaxs = "i",
            xaxs = "i"
        )

        # Lines
        for (i in seq_along(frag_lengths)) {
          lines(
            density(frag_lengths[[i]], from = min_fraglength, to = max_fraglength),
            col = colors[i],
            lwd = 1
          )
        }

        # grid
        abline(v = 150, lty = 2)

        # axes
        # ticksize
        tck = -0.02
        xmgp = c(1.6, 0.5, 0)
        ymgp = c(3, 0.7, 0)
        axis(
            side = 1,
            las = 1,
            tck = tck,
            lwd = 0.5,
            mgp = xmgp
            )
        axis(
            2,
            las = 1,
            tck = tck,
            lwd = 0.5,
            mgp = ymgp
            )
        title(xlab = "Fragment length (bp)", mgp = xmgp)
        title(ylab = "Density", mgp = ymgp)

        # minor ticks
        xat <- seq(from = 0,
                   to = 1.1 * max_fraglength,
                   by = 10)
        axis(
            side = 1,
            at = xat,
            lwd = 0.5,
            tck = tck / 2,
            labels = FALSE
        )
        yat <- seq(from = ylim[1],
            to = ylim[2],
            by = 0.001)
        axis(
            side = 2,
            at = yat,
            lwd = 0.5,
            tck = tck / 2,
            labels = FALSE
        )

        if (box) { box() }

        # legend
        legend(
            legend_pos,
            lables,
            bty = 'n',
            lwd = 2,
            seg.len = 1,
            col = colors
        )
    } else {
        ## if sample_sheet is not presented, plot all bam files in separate pages
        for (i in seq_along(frag_lengths)) {
            plot(
                density(frag_lengths[[i]], from = min_fraglength, to = max_fraglength),
                lwd = 2,
            xlab = "Fragment length (bp)",
            main = gsub(".bam", "", basename(names(
                frag_lengths[i]
        )))
      )

      abline(v = seq(min_fraglength, max_fraglength, 100),
             lty = 2)
        }
    }

  if (!is.null(format) && format == 'pdf') dev.off()
}
