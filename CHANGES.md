# Release notes
All notable changes in `mnaseq` library are documented here.  

## v0.0.2
- x-, y- lables in plotComb fixed, margins increased  
- Ordering for correlation heatmaps inverted to arrange genes from high to low array regularity.  
- Default y-axis min limit is changed to the min value instead of 0 in the AveDyad plot. Ticks locations are improved.  
- Report information are added at Report info.  
- Chapters of the report are reorganized. Descriptions to the parts of report are added.  
- Sample sheet in statDist added. Improved finding sample sheet in the report. 
- rda argument added to the report to prevent creation rda files to reuse existing files.


## v0.0.1
- Changes log file added  
- Licence added  
