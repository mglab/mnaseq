---
title: Mnaseq R package
bibliography: references.bib
output:
  html_document:
    toc: true
    toc_depth: 3
    number_sections: true
    theme: united
    highlight: tango
---
<img src="man/images/logo.svg" align="center" width="500" />

# Introduction
Evenly spaced regular arrays of nucleosomes cover significant portions of the genome and play an important role in regulation and protection the genetic information [@singh_nucleosome_2021].  
MNase-seq  is a technique for the measurement of nucleosome occupancy [@kent_chromatin_2011].  
`mnaseq` is an R package for processing of *Saccharomyces cerevisiae* **MNase sequencing** data.  
The primary data for the `mnaseq` is MNase-seq **aligned and sorted** reads in BAM format.  
Analysis included algorithms described by [Josefina Ocampo](https://www.linkedin.com/in/josefina-ocampo-317b5884/) in her paper [@ocampo_isw1_2016].
The core functions for the *Ocampo analysis* was written by [Tobias Straub](https://www.linkedin.com/in/tobias-straub-97605655/) and taken from [tsTools](https://github.com/musikutiv/tsTools).

# Installation
## Installation from the gitlab 
1. Install `devtools` if not installed. Copy commands into your R script:  
```{r}
install.packages('devtools', dependencies = TRUE)
```

2. Install [**Bioconducor**](https://www.bioconductor.org/install/) and its libraries:

```{r}
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
```
3. Install Bioconductor packages:  

```{r}
BiocManager::install(
    c("GenomicRanges", 
      "rtracklayer", 
      "GenomicAlignments",
      "TxDb.Scerevisiae.UCSC.sacCer3.sgdGene",
      "BSgenome.Scerevisiae.UCSC.sacCer3"), force = TRUE)
```

4. Install `mnaseq` from the gitlab.  

```{r}
require('devtools')
install_gitlab('mglab/mnaseq', force = TRUE)
```

## Installation from sources

Alternatively you can download the package and install it from your local directory.  

```{r}

# Install dependencies if not installed
install.packages(c('colorRamps', 'corrplot', 'zoo', 'e1071'))

git clone https://gitlab.com/mglab/mnaseq.git
tar -czvf  mnaseq.tar.gz mnaseq
install.packages("mnaseq", repos = NULL)
```

# Usage
1. Load the package in R:  

```{r}
library(mnaseq)
```

# Practical example {#practical_example}
The easiest way to try everything is to use an integrated examplel which is a liteweight 
version of the real experiment.  
You will need to copy the following files from `mnaseq` package into your project directory to run this example:  

1. s01_out.bam  
2. s02_out.bam  
3. sac_cer3_genome_annotation.csv  
4. sample_sheet.txt
5. alignment_statistics.tsv  

**Dataset**: two samples *control/s01_out* is a wild type yeast strain with normal 
nucleosome distribution and *test/s02_out* is a mutant yeast strain with disturbed 
nucleosome localizations. Only 50% of reads at chrI were taken from the original data.  
Later you can use your own **bam** files with your own *sample_sheet.txt* file.  

Copy all necessary files into the project directory or use the following code which 
creates the working directory and copy all necessary files there:

```{r}
ext_dir = list.files(paste0(find.package('mnaseq'), '/extdata/example'), full.names = TRUE)
dir.create('test_project/data', recursive = TRUE)
file.copy(ext_dir, 'test_project/data')
```

Alternatively you can directly copy the files from the package directory:
```{r}
list.files(paste0(find.package('mnaseq'), '/extdata/example'), full.names = TRUE)
```

**sample_sheet.txt** is a tab separated file containing information you will need for plots. It contains  following columns:  

| File_names | Lable_names | Colors    |
|------------|-------------|-----------|
| 's01_out'  | 'Control'   | '#045a8d' |
| 's02_out'  | 'Test'      | '#74a9cf' |

*File_names* includes names of the  input bam files without .bam extension.  
*Lable_names* contains names which will be displayed on plots.  
*Colors* defines colors for our samples in various plots.  

The **sample_sheet.txt** file should be manually specified for each experiment. 
*Sample sheet* is *not mandatory*. If it is not presented, file names will 
be used for plot labels and colors for samples will be automatically selected for your samples.  

**sac_cer3_genome_annotation.csv** file contains genomic positions for choosen 
genes with good nucleosome arrays. You can reuse this file for your own experiments.  
**alignment_statistics.tcv** is a tab delimited text file generated during sequencing 
fastq files processing and showing statistics of sequencing, alignment and filtering 
of reads.  

## Generate report
You can use `report` function which will generate all necessary data, process it 
and make an html report for you.  

```{r}
mnaseq::report(
    bamdir = 'test_project',
    outdir = 'test_project/plots')
```

If for some reasons it does not work or you wish to process your data step by 
step you can process your data manually as described bellow in the chapter [**Data
processing**](#data_processing).  


## Analysis of subnucleosomes
`report` function will select fragments which corresponds to expected nucleosomal 
fragments from 130 bp to 160 bp, assumed that the nucleosome size is 147 bp.  
You can also select shorter fragments for the analysis of subnucleosomes, 
also known as 'fragile nucleosomes' or 'hexasomes' [@ramachandran_transcription_2017]:  

```{r}
# Sub-nucleosomal fragmetns
mnaseq::report(
    bamdir = 'test_project/data',
    outdir = 'test_project/report_80_130bp',
    rdadir = 'test_project/rda_80_130bp',
    args = list(min_fraglength = 80
               , max_fraglength = 130
               , cores = 4L)
               )
```

## Data processing {#data_processing}
`report` function generates all necessary data and plots for all samples. You can also 
process data and make plots separetely for different samples.

Here is how data processing works:  
The key function is `bam2rda` which utilizes sequentially other functions: 
`bam2bwr`, `tssCenteredDensity` and `nrl`.  

1. `bam2bwr` function convert **bam** files into *coverages* using `bam2dyad` 
function and save *coverages* as **bigwig** files.  
2. `tssCenteredDensity` function takes coverages in **bigwig** files, centrifies 
coverages around TSS for genes selected in `ORF` argument and saves matrices 
(genes x coverages) as **rda** data files.  
3. `nrl` function calculates three *Ocampo* metrics: **nucleosome repeat length**, 
**array regularity** and **shift** of array from theoretically calculated 'ideal' 
nucleosome array. Result is saved as **rda** data file.  

You can generate data manually using `bam2bwr` function wich converts alignments 
in *bam* files into *Rda* data files. These *Rda* data files will be later used 
for different plots described in chapter [Main Plots](#main_plots).  
*Rda* files as well as *BigWig* files of read coverages will be generated by 
`bam2rda` function.  
Calculation takes time.  

```{r}
mnaseq::bam2rda(
  bamdir = 'test_project/data',
  type = 'PE',
  rdadir = 'test_project/rda',
  ORFs = 'test_project/data/sac_cer3_genome_annotation.csv',
  cores = 8L
)
```

See `?bam2rda` or `?mnaseq::bam2rda` for more details.  

## Fragment length plot
**Fragment length plot** shows the distribution of fragment length of your sequencing data.  
This plot takes *bam* files as an input.  
```{r}
mnaseq::plotFragLength(
  bamdir = 'test_project/data',
  outdir = 'test_project/plots',
  format = 'pdf',
  sample_sheet = 'test_project/data/sample_sheet.txt',
  plotname = '01_fragment_length_destribution',
  subset = c('s01_out.bam',
             's02_out.bam')
)
```

See `?plotFragLength` or `?mnaseq::plotFragLength` for more details.  

## Main plots {#main_plots}

The following plots will use the *Rda* data generated by either `report` 
or `bam2rda` functions.  

### Plot dyad density  

```{r}
mnaseq::plotAveDyad(
    rdadir = 'test_project/rda',
    format = 'pdf',
    sample_sheet = 'test_project/data/sample_sheet.txt',
    outdir = 'test_project/plots',
    param = 'l',
    plotname = '02_nucleosome_dyad_density',
    subset = c('s01_out.bam',
               's02_out.bam')
)
```
See `?mnaseq::plotAveDyad` for more parameters.  

### Plot dyad density as hitmaps
In some cases, for example for many samples it is more convenient to make dyad 
densities as hitmaps:  

```{r}
mnaseq::plotAveDyadDens(
    rdadir = 'test_project/rda',
    sample_sheet = 'test_project/data/sample_sheet.txt',
    param = 'l',
    xlim = c(-500,1000)
)
```

See also `?plotAveDyadDens` or `?mnaseq::plotAveDyadDens` for more details.  

### Ocampo correlation plot

```{r}
mnaseq::plotOcampoDens(
    rdadir = 'test_project/rda',
    format = 'pdf',
    outdir = 'test_project/plots',
    plotname = '03_Ocampo_correlation_plot',
    sample_sheet = 'test_project/data/sample_sheet.txt',
    subset = c('s01_out.bam',
               's02_out.bam'),
    param = 'r'
)
```
See `?plotOcampoDens` or `?mnaseq::plotOcampoDens` for more details.  

### Ocampo correlation boxplot

```{r}
mnaseq::plotBoxOcampo(
    rdadir = 'test_project/rda',
    format = 'pdf',
    outdir = 'test_project/plots',
    plotname = '03_Ocampo_correlation_boxplot',
    sample_sheet = 'test_project/data/sample_sheet.txt',
    subset = c('s01_out.bam',
             's02_out.bam'),
    param = 'r'
)
```

See `?plotBoxOcampo` or `?mnaseq::plotBoxOcampo` for more details.  

### Ocampo correlation density as hitmaps
In the case of many samples it is more convenient to make `plotAveDyadDens` plot 
instead of `plotAveDyad` plot:

```{r}
mnaseq::plotAveDyadDens(
    rdadir = 'test_project/rda',
    sample_sheet = 'test_project/data/sample_sheet.txt',
    param = 'r',
    xlim = c(-500,1000)
)
```

See also `?plotAveDyadDens` or `?mnaseq::plotAveDyaadDens` for more details.  

### Statistics of Ocampo correlation

```{r}
table <- statDist(
    indir = 'test_project/rda'),
    prefix = 'ocampo2.res_',
    column = 'r')

write.table(
    table, 
    file = 'test_project/data/ocampo_spacing_stat.csv', 
    sep = '\t', col.names = NA, quote = FALSE)
```

See `?statDist` or `?mnaseq::statDist` for more details.  

### Ocampo spacing plot

```{r}
mnaseq::plotOcampoDens(
    rdadir = 'test_project/rda',
    format = 'pdf',
    outdir = 'test_project/plots',
    plotname = "04_Ocampo_spacing_plot",
    sample_sheet = 'test_project/data/sample_sheet.txt',
    subset = c('s01_out.bam',
               's02_out.bam'),
    param = 'l'
)
```

See `?plotOcampoDens` or `?mnaseq::plotOcampoDens` for more details.  

### Statistics of Ocampo spacing

```{r}
table <- statDist(
    indir = 'test_project/rda'),
    prefix = 'ocampo2.res_',
    column = 'space')

write.table(
    table, 
    file = 'test_project/data/ocampo_spacing_stat.csv', 
    sep = '\t', col.names = NA, quote = FALSE)
```

See `?mnaseq::statDist` for more details.  

### Ocampo spacing boxplot
```{r}
mnaseq::plotBoxOcampo(
    rdadir = 'test_project/rda',
    format = 'pdf',
    outdir = 'test_project/plots',
    plotname = '03_Ocampo_spacing_boxplot',
    sample_sheet = 'test_project/data/sample_sheet.txt',
    subset = c('s01_out.bam',
               's02_out.bam'),
    param = 'l'
)
```

See `?plotBoxOcampo` or `?mnaseq::plotBoxOcampo` for more details.  

### Heat map plots
Heat map plots can be generated for both parameters, the spacing length `param = 'l'` 
and correlation `param = 'r'`.  

Heatmaps for the correlation:  
```{r}
mnaseq::plotHeatmap(
    rdadir = 'test_project/rda',
    format = 'pdf',
    outdir = 'test_project/plots',
    sample_sheet = 'test_project/data/sample_sheet.txt',
    param = 'r'
)
```

Heatmaps for the spacing lenth:  
```{r}
mnaseq::plotHeatmap(
    rdadir = 'test_project/rda',
    format = 'pdf',
    outdir = 'test_project/plots',
    sample_sheet = 'test_project/data/sample_sheet.txt',
    param = 'l'
)
```

See `plotHeatmap` or `?mnaseq::plotHeatmap` for more details.  

### Coverage plot
Read coverage signals saved in *BigWig* format and located in *rda* directory.  
To get *BigWig* coverages use the `bam2bw` function. This function is also executed inside of the `bam2rda` function we run earlier. 
See Chapter [Data processing](#data_processing).  
To visualize coverage signals for the specified region use `plotCov` function:  

```{r}
mnaseq::plotCov(
    indir = 'test_project/rda',
    format = 'pdf',
    outdir = 'test_project/plots',
    y_lim = 30,
    chr = "chrI",
    start = 50000,
    end = 80000,
    sample_sheet = 'test_project/data/sample_sheet.txt',
    subset = c('s01_out.bam',
               's02_out.bam')
)
```
See `?mnaseq::plotCov`, `?mnaseq::bam2rda` and `?mnaseq::bam2bw` for more details.  

# Real experiment

Use *sac_cer3_genome_annotation.csv* in `bam2rda` function for the `ORFs` argument.  

**Sample sheet** should be manyally specified for each experiment as it is shown 
in the chapter [Practical Example](#practical_example).  

Here is an example how you can organize your project:  
```
└── project_name
    ├── dat
    │   ├── s01_out.fastq
    │   └── s02_out.fastq
    ├── out
    │   ├── bam
    │   │   ├── s01_out.bam
    │   │   ├── s02_out.bam
    │   │   └── sample_sheet.txt
    │   ├── plots
    │   └── rda
    ├── scr
    │   ├── fastq2bam.sh
    │   └── my_script.R
    └── sup
        └── sac_cer3_genome_annotation.csv
```

**project_name** - project directory containing other subdirectories:  
**dat** - directory for original fastq data.  
**scr** - directory for scripts.  
**out** - output directory for processed raw files and reports.  
**sup** - directory containing supplementary files (annotations, descriptions and so on).  
**bam** - directory for alignment bam files.  
**plots** - directory for plots.  
**rda** - directory for R Data files and generated bw files.  

# Analysis description
Nucleosome arrays with different distances between nucleosomes are simulated for each gene. 
The real MNase-seq data compared with simulated data to find the closest simulated nucleosome 
array by calculation correlation between real data and simulated data.  
Parameters of the closest simulated data applied to the real data to receive three important 
metrics of the nucleosome array:  
1. **nucleosome repeat length** - distance between nucleosomes.  
2. **array regularity** - correlation coefficient between real data and simulated data. The value
closer to 1 means better array regularity or better correlation with the 'ideal' simulated nucleosome 
array.  
3. **shifts** - shifts from the nucleosomes from the real data and simulated nucleosomes.  

# Citation of the mnaseq R package
To site the `mnaseq` package please cite our last paper [@singh_biogenesis_2021]:  
Ashish Kumar Singh, Tamás Schauer, Lena Pfaller, Tobias Straub & Felix Mueller-Planitz. 
The biogenesis and function of nucleosome arrays. Nature Communications, 2021 
Dec 1;12(1):7011. doi: [10.1038/s41467-021-27285-6](https://doi.org/10.1038/s41467-021-27285-6).  

# Credits
The core functions for the *Ocampo analysis* [@ocampo_isw1_2016] was written by 
[Tobias Straub](https://www.linkedin.com/in/tobias-straub-97605655/).
Adding functionality for the real application was done by 
[Ashish Singh](https://www.linkedin.com/in/ashish-singh-aa425823/).  
Improvement, documentation and packaging was done by 
[Mark Boltengagen](https://www.linkedin.com/in/mark-boltengagen-2052081a8/).  
Other people from the laboratory of Prof. Dr. **Felix Müller-Planitz** also contributed to
the improvement of the `mnaseq` R package.  

# References
