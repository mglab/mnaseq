#!/bin/bash

# DESCRIPTION: Bam from fastq for MNase-seq nucleosome ocupancy
# AUTHOR: Mark Boltengagen
# VERSION: v2020-08-29

#SBATCH --partition=haswell
#SBATCH --time=02:00:00
#SBATCH --job-name aln
#SBATCH --nodes=2
#SBATCH --ntasks=40
#SBATCH --cpus-per-task=1
#SBATCH --mem 128G
#SBATCH -o /scratch/ws/0/mabo614c-ngs_seq/out/%j.out
#SBATCH -e /scratch/ws/0/mabo614c-ngs_seq/out/%j.err

module() { eval `/mnt/data1/apps/modules/modules-4.4.1/build/bin/modulecmd bash $*`; }
module use /mnt/data1/modulefiles
module load fastqc-0.11.9
module load bowtie2-2.4.1
module load gatk-4.1.8.1
module load pigz-2.4
module load bedtools-2.29.2
module load samtools-1.10

######################################################################
##### PARAMETERS
### Input directory
FASTQ=/mnt/data1/markb/projects/prj15_mona/i01_primary_processing/dat/raw
### Output directory
OUT=/mnt/data1/markb/projects/prj15_mona/i01_primary_processing/out

### Supplementary files
EXCLUDE=/mnt/data1/db/Saccharomyces_cerevisiae/UCSC/sacCer3/bed_exclude_mitoch_ribosomal_roman/exclude_mitoch_ribosomal_roman.bed
Bowtie2Index=/mnt/data1/db/Saccharomyces_cerevisiae/UCSC/sacCer3/bowtie2index_noMT_roman/SacCer3_index

## Cores
PPN=15

###############################################################################

## Start log
{
start=`date +%s`
###############################################################################
##### 1. FASTQC FASTQ QUALITY TEST

mkdir -p $OUT/fq_qc
cd $FASTQ
printf "$(date)\tSTARTED FASTQ OF RAW FASTQ\n\n"

for item in *.fastq.gz
    do
        ((i=i%$PPN)); ((i++==0)) && wait
        fastqc -t $PPN -o $OUT/fq_qc $item &
        printf "\n"
    done
wait

printf "$(date)\tFINISHED FASTQ OF RAW FASTQ\n\n"
printf "=%.0s" {1..80}; printf "\n\n"

###############################################################################
##### 2. Alignment BOWTIE2 for SacCer3 -PE

mkdir -p $OUT/bam
cd $FASTQ

printf "$(date)\tBOWTIE2 FOR SACCER3 STARTS\n"

for item in *1.fastq.gz
    do
      printf "\n$(date)\tStarted alignment and filtering of ${item}\n"
    fname=${item%1.fastq.gz}

    ### Alignment
    bowtie2 -p $PPN -x $Bowtie2Index -X 500 --very-sensitive --no-discordant --no-mixed -1 $item -2 ${fname}2.fastq.gz \
        | samtools sort -@ $PPN -O bam -o $OUT/bam/${fname}in.bam
    
    ### Mark duplicates
        gatk --java-options -Xmx16G MarkDuplicates \
             --INPUT $OUT/bam/${fname}in.bam \
             --OUTPUT $OUT/bam/${fname}md.bam \
             --METRICS_FILE $OUT/bam/${fname}md_metrics.txt \
             --VALIDATION_STRINGENCY SILENT \
             --ASSUME_SORTED true \
             --REMOVE_DUPLICATES false
        
        ### Remove duplicates
        samtools view -h -b -F 1024 $OUT/bam/${fname}md.bam > $OUT/bam/${fname}rd.bam
        
        ### Filter pass quality and properly aligned
        samtools view -@ $PPN -hb -f 2 -F 1804 -q 20 $OUT/bam/${fname}rd.bam \
               | samtools sort -@ $PPN - \
               | samtools view -@ $PPN -bL $EXCLUDE > $OUT/bam/${fname}out.bam

        printf "$(date)\tCompleted alignment and filtering of ${item}\n"
    done

### Index bam files
cd $OUT/bam && for bam in *.bam; do samtools index $bam; done

################################################################################
##### GET READ STATISTICS

mkdir -p $OUT/align_stats
echo -e "Name                   \t \
         R1_reads               \t \
         R2_reads               \t \
         Total_reads            \t \
         Mapped                 \t \
         Pass_quality           \t \
         Rate_pass_quality      \t \
         Mapped_pairs           \t \
         Rate_properly_paired   \t \
         After_rm_duplicates    \t \
         Rate_duplicates        \t \
         Filtered               \t \
         Rate_filtered          \t \
         Mitochondrial          \t \
         Rate_mit               \t \
         Rate_all_filters       \t \
         NRF                    \t \
         PBC1                   \t \
         PBC2                     " | tr -d ' ' >> $OUT/align_stats/alignment_statistics.tsv

cd $OUT/bam
for item in *out.bam
    do
        printf "$(date)\tCalculate alignment statistics for ${item%out.bam}\n"
        # Calculate PBC statistics and save into file
        # sort marked duplicates bam by names
        samtools sort -@ $PPN -n -O BAM -o tmp.bam ${item%out.bam}md.bam
        
        # Calculate PBC1, PBC2, NRF
        bedtools bamtobed -bedpe -i tmp.bam | awk 'BEGIN{OFS="\t"}{print $1,$2,$4,$6,$9,$10}' \
            | grep -v 'chrM' | sort | uniq -c | awk 'BEGIN{mt=0;m0=0;m1=0;m2=0}($1==1){m1=m1+1} \
              ($1==2){m2=m2+1} {m0=m0+1} {mt=mt+$1} \
              END{printf "%d\t%d\t%d\t%d\n", mt,m0,m1,m2}' > $OUT/align_stats/${item%md.bam}.pbc.qc
        rm tmp.bam

        read -r -a vars < $OUT/align_stats/${item%md.bam}.pbc.qc
        mt=$(echo ${vars[0]})
        m0=$(echo ${vars[1]})
        m1=$(echo ${vars[2]})
        m2=$(echo ${vars[3]})

        r1_reads=$(unpigz -c $FASTQ/${item%out.bam}1.fastq.gz | awk '(NR-2)%4 ==0 {total++}END{print total}')
        r2_reads=$(unpigz -c $FASTQ/${item%out.bam}2.fastq.gz | awk '(NR-2)%4 ==0 {total++}END{print total}')
        total_reads=$(( $r1_reads + $r2_reads ))
        mapped=$(samtools view -@ $PPN -c $OUT/bam/${item%out.bam}in.bam)
        qpass=$(samtools view -@ $PPN -c -q 30 $OUT/bam/${item%out.bam}in.bam)
        rate_qpass=$(echo "scale=4; $qpass/$mapped" | bc)
        proper_pairs=$(samtools view -@ $PPN -c -f 2 -F 1804 $OUT/bam/${item%out.bam}in.bam)
        rate_proper_pairs=$(echo "scale=4; $proper_pairs/$mapped" | bc)
        rmdup=$(samtools view -@ $PPN -c $OUT/bam/${item%out.bam}rd.bam)
        rate_duplicates=$(echo "scale=4; $rmdup/$mapped" | bc)
        filtered=$(samtools view -@ $PPN -c $OUT/bam/$item)
        rate_filtered=$(echo "scale=4; $filtered/$rmdup" | bc)
        mit=$(samtools idxstats -@ $PPN $OUT/bam/${item%out.bam}rd.bam | grep 'chrM' | cut -f 3)
        rate_mit=$(echo "scale=4; $mit/$rmdup" | bc)
        rate_all_filters=$(echo "scale=4; $filtered/$mapped" | bc)
        nrf=$(echo "scale=4; $m0/$mt" | bc)
        pbc1=$(echo "scale=4; $m1/$m0" | bc)
        pbc2=$(echo "scale=4; $m1/$m2" | bc)

        echo -e "${item%out.bam}     \t \
                 $r1_reads           \t \
                 $r2_reads           \t \
                 $total_reads        \t \
                 $mapped             \t \
                 $qpass              \t \
                 $rate_qpass         \t \
                 $proper_pairs       \t \
                 $rate_proper_pairs  \t \
                 $rmdup              \t \
                 $rate_duplicates    \t \
                 $filtered           \t \
                 $rate_filtered      \t \
                 $mit                \t \
                 $rate_mit           \t \
                 $rate_all_filters   \t \
                 $nrf                \t \
                 $pbc1               \t \
                 $pbc2                " | tr -d ' ' >> $OUT/align_stats/alignment_statistics.tsv

        printf "$(date)\tFinished to calculate alignment statistics for ${item%out.bam}\n"
    done

printf "$(date)\tPIPELINE IS COMPLETE\n\n"

end=`date +%s`
runtime=$((end-start))
echo $runtime
} 2>&1 | tee ${0}.log
